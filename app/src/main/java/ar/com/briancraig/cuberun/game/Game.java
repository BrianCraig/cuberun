package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.Camera;
import com.badlogic.gdx.graphics.g3d.ModelInstance;

import java.util.List;

/**
 * Created by brian on 1/15/17.
 */

public class Game {
    private float speed = 3f;
    private MyCamera camera = new MyCamera();
    private ModelsManager modelsManager;
    private Player player;
    private CubesManager cubesManager;
    private static Game instance = null;
    private int steps = 0;

    enum Difficulty{No, Easy, Medium, Hard};

    static Game getInstance() {
        return instance;
    }

    public Game(ModelsManager modelsManager) {
        instance = this;
        this.modelsManager = modelsManager;
        player = new Player();
        cubesManager = new CubesManager();
    }

    public void update(float delta) {
        delta *= speed;
        cubesManager.update(delta);
        player.update(delta);
        camera.update(delta);
    }

    Difficulty getDifficulty(){
        if(steps< 10){
            return Difficulty.No;
        }else{
            return Difficulty.Easy;
        }
    }

    public MyCamera getCamera() {
        return camera;
    }

    ModelsManager getModelsManager() {
        return modelsManager;
    }

    public void turnRight() {
        player.turnRight();
    }

    public void turnLeft() {
        player.turnLeft();
    }

    public List<ModelInstance> modelInstances() {
        List<ModelInstance> instances = cubesManager.modelInstances();
        instances.add(player.getModelInstance());
        return instances;
    }

    void playerReachedPosition(Position pos) {
        steps++;
        if (!cubesManager.hasCube(pos)) {
            player.kill();
        }
    }

    void end() {
        Gdx.app.exit();
    }
}


package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.math.MathUtils;
import com.badlogic.gdx.math.Matrix4;
import com.badlogic.gdx.math.Quaternion;
import com.badlogic.gdx.math.Vector3;

import java.util.LinkedList;

/**
 * Created by brian on 1/15/17.
 */

class Player {
    private enum Dir {
        Left, Right, Center
    }

    private enum State {
        Running, Waiting, SwitchingTracks, Falling
    }

    private LinkedList<Dir> directions = new LinkedList();
    private Dir dir = Dir.Center;
    private State state = State.Running;
    private Position position = new Position(0, 0);
    private ModelInstance instance;
    private float movementDone = 0f;
    private float falledBlocks = 0f;


    Player() {
        instance = new ModelInstance(Game.getInstance().getModelsManager().getPlayerModel());
        instance.transform.translate(0f, 1f, 0f);
    }

    ModelInstance getModelInstance() {
        return instance;
    }

    void update(float delta) {
        delta *= 2; // our player does two interactions, so it needs to double the movement
        while (delta > 0f) {
            delta -= move(delta);
        }
        applyTransformation();
    }

    void turnRight() {
        directions.add(Dir.Right);
    }

    void turnLeft() {
        directions.add(Dir.Left);
    }

    void kill() {
        Fall();
    }

    private void applyTransformation() {
        instance.transform = new Matrix4(getPosition(), getRotation(), new Vector3(1f, 1f, 1f));
    }

    private float move(float delta) {
        float prevMovementDone = movementDone;
        movementDone = Math.min(movementDone + delta, 1f);
        if (movementDone == 1f) {
            switchMovements();
            return 1f - prevMovementDone;
        } else {
            return delta;
        }
    }

    private void switchMovements() {
        movementDone = 0f;
        if (isRunning()) {
            if (directions.isEmpty()) {
                Wait();
            } else {
                dir = directions.removeFirst();
                SwitchTracks();
            }
            position.addDisplacement(1);
        } else if (isWaiting()) {
            Run();
        } else if (isSwitchingTracks()) {
            if (isGoingRight()) {
                position.addLane(1);
            } else {
                position.addLane(-1);
            }
            Run();
            GoCenter();
        } else if (isFalling()) {
            falledBlocks += 1f;
            if (falledBlocks > 9f) {
                Game.getInstance().end();
            }
        }
        reachedNewBlock();
    }

    private void reachedNewBlock() {
        Game.getInstance().playerReachedPosition(position);
    }

    private Vector3 getPosition() {
        return (new Vector3(getTrackPoint(), getHeightPoint(), getRunningPoint()));
    }

    private float getTrackPoint() { // on Tracks axis
        if (isSwitchingTracks()) {
            if (isGoingRight()) {
                return position.getLane() + movementDone;
            } else { // left, semi-obvious
                return position.getLane() - movementDone;
            }
        } else {
            return position.getLane();
        }
    }

    private float getHeightPoint() { // height of the player Based on the turns
        if (isRunning() || isSwitchingTracks()) {
            return 1f + (MathUtils.sin(movementDone * (MathUtils.PI)) * .5f);
        } else if (isFalling()) {
            return 1f - movementDone - falledBlocks;
        } else {
            return 1f;
        }
    }

    private float getRunningPoint() { // On running axis
        if (isRunning()) {
            return position.getDisplacement() + movementDone;
        } else {
            return position.getDisplacement();
        }
    }

    private Quaternion getRotation() {
        float rotation = movementDone * 90f;
        if (isSwitchingTracks()) {
            if (isGoingRight()) {
                return new Quaternion(new Vector3(0f, 0f, -1f), rotation);
            } else {
                return new Quaternion(new Vector3(0f, 0f, 1f), rotation);
            }
        } else if (isRunning()) {
            return new Quaternion(new Vector3(1f, 0f, 0f), rotation);
        } else {
            return new Quaternion(new Vector3(0f, 0f, 0f), 0);
        }

    }

    private boolean isRunning() {
        return state == State.Running;
    }

    private boolean isWaiting() {
        return state == State.Waiting;
    }

    private boolean isSwitchingTracks() {
        return state == State.SwitchingTracks;
    }

    private boolean isFalling() {
        return state == State.Falling;
    }

    private boolean isGoingRight() {
        return dir == Dir.Right;
    }

    private boolean isGoingLeft() {
        return dir == Dir.Left;
    }

    private boolean isGoingCenter() {
        return dir == Dir.Center;
    }

    private void Run() {
        state = State.Running;
    }

    private void Wait() {
        state = State.Waiting;
    }

    private void SwitchTracks() {
        state = State.SwitchingTracks;
    }

    private void Fall() {
        state = State.Falling;
    }

    private void GoRight() {
        dir = Dir.Right;
    }

    private void GoLeft() {
        dir = Dir.Left;
    }

    private void GoCenter() {
        dir = Dir.Center;
    }

}

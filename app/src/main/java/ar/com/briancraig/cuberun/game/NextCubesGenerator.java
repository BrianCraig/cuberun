package ar.com.briancraig.cuberun.game;

import java.util.HashSet;
import java.util.LinkedList;
import java.util.Random;

import ar.com.briancraig.cuberun.game.Game.Difficulty;

/**
 * @author Bri <3
 * The only inteface of this class is the getNextRound message which returns a set of positions
 */

public class NextCubesGenerator {
    private LinkedList<Node> currentNodes = new LinkedList<>();
    private Random random = new Random();

    public HashSet<Integer> getNodesPositions() {
        positions = new HashSet<>(positions);
        for(Node n: currentNodes){
            switch(n.action){
                case Branch:
                    positions.add(n.position-1);
                    positions.add(n.position);
                    positions.add(n.position+1);
                    break;
                case Merge:
                    positions.add(n.position);
                    positions.add(n.nextPosition());
                    break;
                case Continue:
                    positions.add(n.position);
                    positions.add(n.nextPosition());
                    break;
            }
        }
        return positions;
    }

    enum Dir {
        Left, Right, Center
    }

    enum Action {
        Merge, Branch, Expand, Continue
    }

    // volatile properties
    private LinkedList<Node> nextNodes;
    private HashSet<Integer> positions;
    private int nodes;
    private Game.Difficulty dif;
    private Action action;

    NextCubesGenerator(){
        currentNodes.add(new Node(0, Action.Continue, Dir.Center));
    }

    HashSet<Integer> getNextRound(){
        // we prepare the variables for the round
        prepareRound();
        // we try to set a state to some random nodes
        tryToSetState();
        // and we continue with
        checkForExpansionNeeds();
        //we generate the new nodes
        generateNewNodes();
        // take the positions
        positions = getNodesPositions();
        // and we end round
        endRound();
        return positions;
    }

    private void generateNewNodes() {
        // Well, this is difficult, you know
        // because
        // Continue nodes will continue where they are going
        // Branch nodes will Make two new branches
        // But fucking Merge
        // merge will fuck up things
        // if you encounter a merge node, next one HAS to be a merge too, and doesn't mean its going to merge this round, maybe next
        // fuck i'll code
        for(int i = 0; i < nodes; i++){
            Node node = currentNodes.get(i);
            switch (node.action){
                case Continue:
                    nextNodes.add(new Node(node.nextPosition(), Action.Continue, Dir.Center));
                    break;
                case Branch:
                    nextNodes.add(new Node(node.position-1, Action.Continue, Dir.Center));
                    nextNodes.add(new Node(node.position+1, Action.Continue, Dir.Center));
                    break;
                case Merge:
                    Node nextNode = currentNodes.get(i+1);
                    switch(nextNode.position - node.position){
                        case 1:
                            //the two are together
                            int position = node.position;
                            if(random.nextBoolean()) {
                                position++;
                            }
                            nextNodes.add(new Node(position, Action.Continue, Dir.Center));
                            break;
                        case 2:
                            //perfect union
                            nextNodes.add(new Node(node.position+1, Action.Continue, Dir.Center));
                            break;
                        default:
                            //just more than two spaces between them
                            nextNodes.add(new Node(node.nextPosition(), Action.Merge, Dir.Right));
                            nextNodes.add(new Node(nextNode.nextPosition(), Action.Merge, Dir.Left));
                            break;
                    }
                    i++; // remember to evade next node
                    break;
            }
        }
    }

    private void checkForExpansionNeeds() {
        //later LOL
        // well, you must expand all the nodes in-between distances, so you can Branch many without problems
        // like,
    }

    private void tryToSetState() {
        // select 2 adjacent
        switch (action){
            case Branch:
                Node n = currentNodes.get(random.nextInt(nodes));
                // select one node at random, if has 2+ spaces on left & right, and its free, Branch it
                if(!n.isFree())return;
                if(leftDistance(n) < 2 )return;
                if(rightDistance(n) < 2 )return;
                //ok
                n.action = Action.Branch;
                break;
            case Merge:
                if(nodes == 1)return; //never EVER try this with one node
                // if all two adjacent are free, set to merge them
                int rid = random.nextInt(nodes-1);
                Node n1 = currentNodes.get(rid);
                Node n2 = currentNodes.get(rid+1);
                if(!n1.isFree())return;
                if(!n2.isFree())return;
                //ok
                n1.dir = Dir.Right;
                n2.dir = Dir.Left;
                n1.action = Action.Merge;
                n2.action = Action.Merge;
                break;
        }
    }

    private int leftDistance(Node n) {
        Node l = null;//last but not least
        for(Node x: currentNodes){
            if(n==x){
                if(l == null){// is the first
                    return Integer.MAX_VALUE;
                }else {
                    return x.position - l.position;
                }
            }
            l = x;
        }
        return 0; // never gets here (insaneee)
    }

    private int rightDistance(Node n) {
        Node s = null;//searched one
        for(Node x: currentNodes){
            if(s!=null){ // next one got!
                return x.position - s.position;
            }
            if(n==x){
                s = n;
            }
        }
        return Integer.MAX_VALUE; // no next one, goinsane!
    }

    private Action roundAction(){
        if(dif == Difficulty.No){
            return Action.Continue;
        }
        if (random.nextFloat() < 0.8){
            return Action.Continue;
        }else{
            if(random.nextBoolean()){
                return Action.Branch;
            }else{
                return Action.Merge;
            }
        }

    }

    private void prepareRound(){
        positions = new HashSet<>();
        nextNodes = new LinkedList<>();
        nodes = currentNodes.size();
        dif = Game.getInstance().getDifficulty();
        action = roundAction();
    }

    private void endRound(){
        currentNodes = nextNodes;
    }

    private class Node{
        public int position;
        public Dir dir;
        public Action action;

        Node(int pos,Action a, Dir d){
            position = pos;
            action = a;
            dir = d;
        }

        int nextPosition(){
            if(dir == Dir.Center) return position;
            if(dir == Dir.Left) return position-1;
            if(dir == Dir.Right) return position+1;
            throw new Error(); // should never get here
        }

        public boolean isFree() {
            return action == Action.Continue;
        }
    }


}

package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.PerspectiveCamera;

/**
 * @author brian
 */

public class MyCamera extends PerspectiveCamera {
    private float displacement = 0f;

    MyCamera() {
        super(67, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        position.set(-7f, 5f, -4f);
        lookAt(0, 0, 0);
        near = 1f;
        far = 300f;
        update();
    }

    void update(float delta) {
        displacement += delta;
        lookAt(displacement);
    }

    private void lookAt(float position) {
        this.position.z = position - 4.0f;
        lookAt(0, 0, position);
        update();
    }

    public void setDimensions(int width, int height){
        this.viewportWidth = width;
        this.viewportHeight = height;
        update();
    }

}

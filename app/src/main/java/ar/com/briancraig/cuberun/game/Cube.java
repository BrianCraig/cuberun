package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.ModelInstance;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;

import java.util.Random;

/**
 * Created by brian on 1/15/17.
 */

public class Cube extends Position {
    private ModelInstance instance;
    public static Random random = new Random();
    private float colorStrong = 0.7f + random.nextFloat() * 0.3f;

    public Cube(int lane, int displacement, Model model) {
        super(lane, displacement);
        instance = new ModelInstance(model);
        instance.transform.setToTranslation(lane, (float) 0, displacement); // var x is along the axis, var y is for running
        instance.materials.get(0).set(ColorAttribute.createDiffuse(new Color(0f, colorStrong, colorStrong, 1f)));
    }

    public ModelInstance getInstance() {
        return instance;
    }

    public void setAlpha(float x) {
        instance.materials.get(0).set(ColorAttribute.createDiffuse(new Color(0f, colorStrong, colorStrong, x)));
    }
}

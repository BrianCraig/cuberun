package ar.com.briancraig.cuberun.game;

/**
 * @author brian
 *         The Position describes where a Cube is placed, it has two Axis, one
 *         is the lane or the track, the other is the
 */

class Position {
    private int lane;
    private int displacement;

    Position(int lane, int displacement) {
        this.lane = lane;
        this.displacement = displacement;
    }

    int getDisplacement() {
        return displacement;
    }

    void setDisplacement(int displacement) {
        this.displacement = displacement;
    }

    void addDisplacement(int displacement) {
        this.displacement += displacement;
    }

    int getLane() {
        return lane;
    }

    void setLane(int lane) {
        this.lane = lane;
    }

    void addLane(int lane) {
        this.lane += lane;
    }

    boolean equals(Position p2) {
        return (p2.getDisplacement() == getDisplacement() && p2.getLane() == getLane());
    }
}

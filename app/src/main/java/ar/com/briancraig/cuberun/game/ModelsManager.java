package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.graphics.Color;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.VertexAttributes;
import com.badlogic.gdx.graphics.g3d.Material;
import com.badlogic.gdx.graphics.g3d.Model;
import com.badlogic.gdx.graphics.g3d.attributes.BlendingAttribute;
import com.badlogic.gdx.graphics.g3d.attributes.ColorAttribute;
import com.badlogic.gdx.graphics.g3d.utils.ModelBuilder;

public class ModelsManager {
    private ModelBuilder modelBuilder = new ModelBuilder();
    private Model cubeModel;
    private Model playerModel;

    public ModelsManager() {
        cubeModel = modelBuilder.createBox(0.999f, 0.999f, 0.999f,
                new Material(ColorAttribute.createDiffuse(new Color(1f, 1f, 1f, 1f)), new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)), // DONT remove diffuse or blending attributes because it makes one shader per instance
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);

        playerModel = modelBuilder.createBox(0.999f, 0.999f, 0.999f,
                new Material(ColorAttribute.createDiffuse(new Color(181f / 255f, 68f / 255f, 238f / 255f, 1f)), new BlendingAttribute(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA)), // #B544EE
                VertexAttributes.Usage.Position | VertexAttributes.Usage.Normal);
    }

    public Model getCubeModel() {
        return cubeModel;
    }

    public Model getPlayerModel() {
        return playerModel;
    }

    public void dispose() {
        cubeModel.dispose();
        playerModel.dispose();
    }
}

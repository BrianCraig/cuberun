package ar.com.briancraig.cuberun;

import com.badlogic.gdx.ApplicationAdapter;
import com.badlogic.gdx.Gdx;
import com.badlogic.gdx.graphics.GL20;
import com.badlogic.gdx.graphics.g3d.ModelBatch;

import ar.com.briancraig.cuberun.game.Game;
import ar.com.briancraig.cuberun.game.ModelsManager;

/**
 * Created by brian on 1/15/17.
 */

class Application extends ApplicationAdapter {
    private ModelBatch modelBatch;
    private ModelsManager modelsManager;
    private Game game;
    private ApplicationInput applicationInput;
    private ApplicationFPS applicationFPS;
    private float pauseSeconds = 2f;

    @Override
    public void create() {
        prepareOpenGL();
        modelsManager =  new ModelsManager();
        modelBatch = new ModelBatch();
        game = new Game(modelsManager);
        applicationInput = new ApplicationInput(game);
        Gdx.input.setInputProcessor(applicationInput);
        applicationFPS = new ApplicationFPS();
    }

    @Override
    public void render() {
        updateGame();
        preRender();

        modelBatch.begin(game.getCamera());
        modelBatch.render(game.modelInstances());
        modelBatch.end();

        applicationFPS.draw();
    }

    private void updateGame() {
        float delta = Gdx.graphics.getDeltaTime();
        pauseSeconds-=delta;
        if(pauseSeconds <= 0)game.update(delta);
    }

    @Override
    public void resize (int width, int height) {
        game.getCamera().setDimensions(width, height);
    }

    private void prepareOpenGL() {
        Gdx.gl.glClearColor(0, 0, 0, 1);
        Gdx.gl.glEnable(GL20.GL_BLEND);
        Gdx.gl.glBlendFunc(GL20.GL_SRC_ALPHA, GL20.GL_ONE_MINUS_SRC_ALPHA);
    }

    private void preRender() {
        Gdx.gl.glViewport(0, 0, Gdx.graphics.getWidth(), Gdx.graphics.getHeight());
        Gdx.gl.glClear(GL20.GL_COLOR_BUFFER_BIT | GL20.GL_DEPTH_BUFFER_BIT);
    }

    @Override
    public void dispose() {
        //cubes.dispose();
    }
}



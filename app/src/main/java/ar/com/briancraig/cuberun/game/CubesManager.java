package ar.com.briancraig.cuberun.game;

import com.badlogic.gdx.graphics.g3d.ModelInstance;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Random;

/**
 * Created by brian on 1/15/17.
 */

class CubesManager {
    private List<Cube> cubes = new LinkedList<Cube>();
    private float seeDistance = 14.5f;
    private float sawDistance = -7.5f;
    private int lastRow = -1;
    private float displacement = 0f;
    private NextCubesGenerator posGenerator = new NextCubesGenerator();

    CubesManager() {
        update(0f);
    }

    void update(float delta) {
        displacement += delta;
        updateCubes();
    }

    List<ModelInstance> modelInstances() {
        List<ModelInstance> inst = new LinkedList<ModelInstance>();
        for (Cube c : cubes) {
            inst.add(c.getInstance());
        }
        return inst;
    }

    private void updateCubes() {
        addCubes();
        removeCubes();
        setCubesAlpha();
    }

    private void addCubes() {
        while (lastRow < displacement + seeDistance) {
            lastRow += 1;
            for (Integer i: posGenerator.getNextRound()) {
                cubes.add(new Cube(i, lastRow, Game.getInstance().getModelsManager().getPlayerModel()));
            }
        }
    }

    private void removeCubes() {
        float sawPosition = displacement + sawDistance;
        for (Iterator<Cube> i = cubes.iterator(); i.hasNext(); ) {
            Cube cube = i.next();
            if (cube.getDisplacement() <= sawPosition) {
                i.remove();
            }
        }
    }

    private float cubeRatio(Cube c) {
        return ((c.getDisplacement() - displacement) - sawDistance) / (seeDistance - sawDistance);
    }

    private void setCubesAlpha() {
        for (Cube c : cubes) {
            float r = cubeRatio(c);
            if (r <= 0.10f) {
                c.setAlpha(r * 10f);
            } else if (r >= 0.90f) {
                c.setAlpha((1f - r) * 10f);
            }
        }
    }

    boolean hasCube(Position pos) { //get cube would be better ;) for exxtra logic
        for (Cube c : cubes) {
            if (pos.equals(c)) return true;
        }
        return false;
    }
}

